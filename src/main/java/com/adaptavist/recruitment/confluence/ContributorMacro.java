package com.adaptavist.recruitment.confluence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.VersionHistorySummary;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

@Scanned
public class ContributorMacro implements Macro {

    private final PageManager pageManager;

    @Autowired
    public ContributorMacro(@ComponentImport PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    @Override
    public String execute(Map<String, String> parameters, String bodyContent, ConversionContext ctx) throws MacroExecutionException
    {  
    	//saves all contributors count list
        final Map<String, Integer> contributorsList = new HashMap<String, Integer>();
        
        //pull contributor details for each comment and update to the list
        if(ctx != null)
        {
            List<Comment> comments = ctx.getPageContext().getEntity().getComments();
            if(comments != null)
            {
            	for(Comment comment: comments)
            	{
            		updateContributorCount(contributorsList, comment.getCreator().getFullName());
            	}
            }
        }

        //pull contributor details for version history summary and update to the list
        if(pageManager != null)
        {
            List<VersionHistorySummary> summaryList = pageManager.getVersionHistorySummaries(ctx.getEntity());
            if(summaryList != null)
            {
            	for(VersionHistorySummary summary: summaryList)
            	{
            		updateContributorCount(contributorsList, summary.getLastModifier().getFullName());
            	}
            }
        	
        }
        
        String pageName = "";
        
        //the below code display data in table format
        StringBuilder builder = new StringBuilder();
        builder.append("<p>");
        
        if(Page.class.isInstance(ctx.getEntity())) pageName = "Page";
        if(BlogPost.class.isInstance(ctx.getEntity())) pageName = "BlogPost";
        
    	builder.append("<br> "+pageName+": "+ctx.getPageContext().getPageTitle());
    	builder.append("<br> Created By: "+ctx.getPageContext().getEntity().getCreator().getFullName());
        
    	if(contributorsList.entrySet().size() > 0)
    	{
            builder.append("<table width=\"50%\">");
            builder.append("<tr><th>Contributor Name</th><th>Total Edits</th></tr>");
            for (Map.Entry<String, Integer> entry : contributorsList.entrySet())
            {
                builder.append("<tr>").append("<td>").append(entry.getKey()).append("</td><td>").append(entry.getValue()).append("</td>").append("</tr>");
            }
            builder.append("</table>");
    	}
    	else
    	{
    		builder.append("Contributor Macro Initialised");
    	}
        builder.append("</p>");

        return builder.toString();
    }
    
    /**
     * This method increments the users count on each update for the page or blog post
     * @param list
     * @param user
     */
    private void updateContributorCount(final Map<String, Integer> list, String user)
    {
    	if(list != null)
    	{
    		if(list.containsKey(user))
    		{
    			Integer counter = list.get(user);
    			list.replace(user, (counter+1));
    		}
    		else
    		{
    			list.put(user, 1);
    		}
    	}
    }
    
    @Override
    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }
}