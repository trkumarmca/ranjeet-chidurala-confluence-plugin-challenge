package com.adaptavist.recruitment.confluence;

import static org.mockito.Mockito.when;

import java.util.Arrays;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.adaptavist.recruitment.confluence.ContributorMacro;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;

@RunWith (MockitoJUnitRunner.class)
public class ContributorMacroTest extends TestCase{

    @Mock
    private PageManager pageManager;
    
    @Mock
    private ConversionContext conversionContext;
    
    private ContributorMacro contributorMacro;
    private Page page;
    
    @Before
    public void setUp() throws Exception
    {
        super.setUp();
        
    	// create test page
    	page = new Page();
    	page.setTitle("Page title");
    	
        // set up stub method to return test page
    	when(pageManager.getRecentlyAddedPages(55, "DS")).thenReturn(Arrays.asList(page));
    	
        // create the macro
    	contributorMacro = new ContributorMacro(pageManager);
    }
    
    @Test
	public void testTitle() throws MacroExecutionException {
    	
    	// verify that the output contains the page title
    	/*String output = contributorMacro.execute(new HashMap<String, String>(), "", conversionContext);
    	assertTrue("Output should contain page title but was: " + output, output.contains(page.getTitle()));*/
	}

}
